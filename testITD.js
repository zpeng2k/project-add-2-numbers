function sum(numStr1, numStr2) {
    len1 = numStr1.length
    len2 = numStr2.length

    let res = "" // result
    let red = 0 // redundant
    let i=len1-1, j=len2-1 // start from last index

    while (i>=0 && j>=0) {
        digit1 = numStr1[i] - "0" // convert char to number
        digit2 = numStr2[j] - "0" // ...

        sum = digit1 + digit2 + red
        red = parseInt(sum / 10)
        digit = sum % 10

        res = digit + res;
        i--; j--;
    }

    while (i>=0) {
        digit1 = numStr1[i] - "0"
        sum = digit1 + red
        red = parseInt(sum / 10)
        digit = sum % 10
        res = digit + res;
        i--
    }

    while (j>=0) {
        digit2 = numStr2[j] - "0"
        sum = digit2 + red
        red = parseInt(sum / 10)
        digit = sum % 10
        res = digit + res;
        j--;
    }

    return res
}

console.log(sum("4323", "69567"))